import socket, os, time, json

class Client:

    def __init__(self, host, port, timeout):
        self.host = host
        self.port = port
        self.timeout = timeout
        
        self._create_socket()

    def _create_socket(self):
        """ Creates a socket object """
        
        try:
            # s - a socket object
            self.s = socket.create_connection((self.host, self.port), self.timeout)
            self.s.settimeout(self.timeout)
        except socket.error as err:
            print('Creation error ' + str(err))

    def _close_socket(self):
        """ Close it """
        self.s.close()
        

    def put(self, server_metric, metric, timestamp=int(time.time())):
        """ Sends a put command to the server """
        
        # Command
        line_to_send = self.put.__name__ + ' ' + server_metric+ ' ' + str(metric) + ' ' + str(timestamp) +'\n'
        try:
            # send and receive the data
            self.s.sendall(line_to_send.encode('utf8'))
            ans = self.s.recv(1024)
        except socket.timeout as err:
            print('timeout err ' + str(err)) 
        except socket.error as err:
            print('Put error ' + str(err)) 

    def get(self, key):
        """ Requests some data from the server """
        
        def _return_dict(data):
            """ Makes requested data a dict obj """

            # Get rid of some characters 
            data = data.replace('ok\n', '')
            data = (data.strip()).split('\n')
            d = {}
            for item in data:
                # Make list of words 
                i = item.split()
                d.setdefault(i[0], [])
                # Required dictionary
                d[i[0]].append((int(i[2]), float(i[1])))
            return d
                
        try:
            # Trying to get the data
            # Preparing command to send 'get key\n'
            comm = self.get.__name__ + ' ' + str(key) +'\n'
            # Send it
            self.s.sendall(comm.encode('utf8'))
            # Get the answer
            data = self.s.recv(1024)
            data = data.decode('utf8')
            if data == 'ok\n\n':
                return {} 
            else:
                return _return_dict(data)
        except socket.error as err:
            print('get error ' + str(err)) 
    

            

class ClientError(Exception):
    pass

if __name__=='__main__':

    client = Client('127.0.0.1', 8181, 15)
    client.put('palm.cpu', 214)
    client.put('palm.cpu', 35)
    client.put('palm.cpu', 0)
    client.put('palm.cpu', 3455)
    client.put('palm.cpu', 34)
    client.get('try.cpu')
    client.get('palm.cpu')






