import asyncio, time
bank = []


class Server(asyncio.Protocol):
    """ Asyncio TCP-server """
    
    def connection_made(self, transport):
        self.transport = transport

    def data_received(self, data):
        """ Processing the data """
        
        data = data.decode()
        if 'put' in data:
            data = data.replace('put ', '')

            if data in bank:
                # Send msg to client
                # i would use a set, but right order is required
                self.transport.write('ok\n\n'.encode())
            else:
                bank.append(data)
                self.transport.write('ok\n\n'.encode())
            
        elif 'get' and '*' in data:
            # Handle the get command
            # Msg to send
            to_send = 'ok\n'
            for item in bank:
                to_send += item
            to_send +='\n'
            # Send it
            self.transport.write(to_send.encode())
        elif 'get' in data:
            data = data.strip()
            # get key 
            key = data.split(' ')[1]
            to_send = 'ok\n'
            for item in bank:
                if key in item:
                    to_send += item
            # key exists
            if to_send != 'ok\n':
                to_send+='\n'
                self.transport.write(to_send.encode())
            else:
                # key doesn't exist 
                self.transport.write('ok\n\n'.encode())
        else:
            self.transport.write('error\nwrong command\n\n'.encode())
        
def run_server(host, port):
    """ Runs the server"""
    
    loop = asyncio.get_event_loop()
    coro = loop.create_server(
                    Server,
                    host, port)
    server = loop.run_until_complete(coro)
    
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    
    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()














        

